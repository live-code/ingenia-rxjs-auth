# TODO

- interceptor: catcheerror
- localstorage token
- directive login

----

/schede/1/11/222
/schede/:idSCheda/:idFasc/:idDoc

const schede = [
    {
        id: 1,
        title: 'scheda 1',
        fascicoli: [
            {
                id: 11,
                title: 'fasc 1',
                docs: [
                    { id: 111 },
                    { id: 222 },
                    { id: 333 }
                ]                
            },
            {
                id: 22,
                title: 'fasc 2',
                docs: [
                    { id: 111 },
                ]                
            }
        ]       
    }
]
