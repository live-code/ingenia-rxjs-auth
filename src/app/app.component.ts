import { Component } from '@angular/core';

@Component({
  selector: 'ing-root',
  template: `
    <ing-navbar></ing-navbar>
    <hr>
    <div class="container">
      <router-outlet></router-outlet>
    </div>
  `,
})
export class AppComponent {

}

