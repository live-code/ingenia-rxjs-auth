import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../../core/auth/authentication.service';

@Component({
  selector: 'ing-login',
  template: `
  
    <button (click)="loginHandler('fabio', '123')">Login</button>
  `,
})
export class LoginComponent {
  constructor(private authService: AuthenticationService) { }

  loginHandler(user: string, pass: string): void {
    this.authService.login(user, pass);
  }
}
