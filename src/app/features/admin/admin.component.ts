import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'ing-admin',
  template: `
    <p>
      admin works!
    </p>
  `,
  styles: [
  ]
})
export class AdminComponent implements OnInit {

  constructor(private http: HttpClient) {
    http.get('http://localhost:3000/users')
      .subscribe()
  }

  ngOnInit(): void {
  }

}
