import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../../core/services/theme.service';

@Component({
  selector: 'ing-settings',
  template: `
    
    <ng-container *ngIf="(themeService.theme | async) as theme">
      <button
        [class.activeBtn]="theme === 'dark'"
        (click)="themeService.setTheme('dark')">dark</button>
      <button
        [class.activeBtn]="theme === 'light'"
        (click)="themeService.setTheme('light')">light</button>
    </ng-container>
  `,
  styles: [`
    .activeBtn {
      background-color: darkred;
    }
  `]
})
export class SettingsComponent {
  constructor(public themeService: ThemeService) {
  }
}
