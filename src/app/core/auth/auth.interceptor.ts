import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { iif, Observable } from 'rxjs';
import { AuthenticationService } from './authentication.service';
import { Router } from '@angular/router';
import { first, switchMap, withLatestFrom } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private authService: AuthenticationService, private router: Router) {}
/*
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.authService.token$
      .pipe(
        first(),
        switchMap(token => {
          let res = request;
          if (token) {
            res = res.clone({
              setHeaders: {
                Authorization: 'Bearer ' + token
              }
            });
          }
          return next.handle(res)
        })
      );
  }*/

/*
  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.authService.token$
      .pipe(
        first(),
        switchMap(token => iif(
          () => !!token,
          next.handle(request.clone({ setHeaders: { Authorization: 'Bearer ' + token } })),
          next.handle(request)
        ))
      );
  }*/


  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return this.authService.isLogged$
      .pipe(
        first(),
        withLatestFrom(this.authService.token$),
        switchMap(([isLogged, tk]) => iif(
          () => !!isLogged,
          next.handle(request.clone({ setHeaders: { Authorization: 'Bearer ' + tk } })),
          next.handle(request)
        ))
      );
  }
}
