import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private auth: AuthenticationService, private router: Router) {
  }
  canActivate(): Observable<boolean> {
    return this.auth.isLogged$
      .pipe(
        tap(isLogged => {
          if (!isLogged) {
            this.router.navigateByUrl('/');
          }
        })
      );
  }
  
}
