import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Auth } from './auth';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  data$: BehaviorSubject<Auth> = new BehaviorSubject<Auth>(null);

  constructor(private http: HttpClient, private router: Router) {}

  login(user, pass): void {
    const params = new HttpParams()
      .set('user', user)
      .set('pass', pass);

    this.http.get<Auth>('http://localhost:3000/login', { params })
      .subscribe(res => {
        this.data$.next(res);
        this.router.navigateByUrl('admin');
      });
  }

  logout(): void {
    this.data$.next(null);
  }

  get isLogged$(): Observable<boolean> {
    return this.data$
      .pipe(
        map(auth => !!auth)
      );
  }

  get displayName$(): Observable<string> {
    return this.data$
      .pipe(
        map(auth => auth?.displayName)
      );
  }

  get role$(): Observable<'admin' | 'moderator'> {
    return this.data$
      .pipe(
        map(auth => auth?.role)
      );
  }
  get token$(): Observable<string> {
    return this.data$
      .pipe(
        map(auth => auth?.token)
      );
  }
}
