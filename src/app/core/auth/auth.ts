export interface Auth {
  role: 'admin' | 'moderator';
  displayName: string;
  token: string;
}
