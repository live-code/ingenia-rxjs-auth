import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

export type Theme = 'dark' | 'light';

@Injectable({ providedIn: 'root' })
export class ThemeService {
  private value: BehaviorSubject<Theme> = new BehaviorSubject<Theme>('dark')

  setTheme(value: Theme): void {
    this.value.next(value);
  }

  get theme(): Observable<Theme> {
    return this.value.asObservable();
  }

}
