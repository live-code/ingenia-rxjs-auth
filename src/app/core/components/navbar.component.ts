import { Component, OnInit } from '@angular/core';
import { ThemeService } from '../services/theme.service';
import { AuthenticationService } from '../auth/authentication.service';

@Component({
  selector: 'ing-navbar',
  template: `
    <div class="p-3" [ngClass]="themeService.theme | async">
      <button routerLink="login">login</button>
      <button routerLink="admin" *ngIf="authService.isLogged$ | async ">admin</button>
      <button [routerLink]="'users'">users</button>
      <button routerLink="settings">settings</button>
      <button (click)="authService.logout()" *ngIf="authService.isLogged$ | async">
        logout
        <pre>{{authService.displayName$ | async}}</pre>
      </button>
    </div>
  `,
  styles: [`
    .dark { background-color: #222}
    .light { background-color: #bbb}
  `]
})
export class NavbarComponent {

  constructor(public themeService: ThemeService, public authService: AuthenticationService) { }


}
